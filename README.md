# What is it?

This program is used to profile the reuse distance distribution of a cache access stream.

# How to build

## Requirements

1. gcc or clang
2. boost > 1.54
3. cmake > 3.0

## Build instructions

    mkdir build
    cd build
    cmake ../src
    make

The executable `reuse_distance_profiler` will be generated in the build folder.

# How to use it

## Profiling a single benchmark

    ./build/reuse_distance_profiler -b your_benchmark_name -t your_trace_dir_path -o your_output_dir_path

## Profiling all benchmarks in a trace directory

A python script `gen_data.py` is provided to profiling all the benchmarks in a trace directory.

    python3 gen_data.py -t your_trace_dir_path -o your_output_dir_path -e executabe_path
    
Note that `executabe_path` is the path of `reuse_distance_profiler` executable.

## CopyRight

The size balanced tree implementation is from [Alex's Anthology of Algorithms](https://github.com/Alextrovert/Algorithm-Anthology/blob/master/Section-3-Data-Structures/3.5.3%20Size%20Balanced%20Tree%20(Order%20Statistics).cpp)