cmake_minimum_required(VERSION 3.2)
project(reuse_distance_profiler)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Werror")


find_package( Boost 1.54 COMPONENTS program_options log filesystem iostreams thread system REQUIRED )
if (NOT Boost_FOUND)
    message(FATAL_ERROR "Could not find Boost!")
endif()

include_directories( ${Boost_INCLUDE_DIR} )


set(SOURCE_FILES
        main.cc
        trace_reader.cc trace_reader.hh
        profiler.cc profiler.hh
		size_balanced_tree.hh
)


add_executable(reuse_distance_profiler ${SOURCE_FILES})


TARGET_LINK_LIBRARIES(reuse_distance_profiler ${Boost_LIBRARIES})

