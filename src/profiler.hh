#ifndef __PROFILER_H__
#define __PROFILER_H__

#include <map>
#include <memory>
#include "size_balanced_tree.hh"

struct Block{
  uint64_t tag;
  uint64_t lastURD;
  uint64_t lastARD;
  uint64_t lastTS;
};


class Profiler {

 public:
  // an efficient way to implement stack algorithm
  bool access(uint64_t tag);
  void outputMissesByURD(std::ostream & out);
  void outputMissesByARD(std::ostream & out);
  void outputNumStreaksByURD(std::ostream & out);
  void outputNumStreaksByARD(std::ostream & out);

 private:
  uint64_t total_accesses_{0};   // used to generate stack_priority
  std::map<uint64_t, uint64_t> hits_urd_;   // stack_distance: count
  std::map<uint64_t, uint64_t> hits_ard_;   // absolute_reuse_distance: count
  std::map<uint64_t, int64_t> num_streaks_urd_; //stack_distance: count
  std::map<uint64_t, int64_t> num_streaks_ard_; // absolute_reuse_distance: count
  std::map<uint64_t, std::shared_ptr<Block> > cache_map_;  // tag : eviction_priority
  size_balanced_tree<uint64_t, std::shared_ptr<Block> > stack_; // sort by stack_priority
};

#endif  // __PROFILER_H__
