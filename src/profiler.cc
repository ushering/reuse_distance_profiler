#include "profiler.hh"
#include <iterator>
#include <iostream>
#include <cassert>

using namespace std;

bool
Profiler::access(uint64_t tag) {

  total_accesses_++; // total_accesses_ is the currrent time
  auto map_it = cache_map_.find(tag);

  if(map_it != cache_map_.end()) { // hit

    auto blk = map_it->second;
    uint64_t last_ts = blk->lastTS;

    // calculate stack_distance
    uint64_t cur_urd = stack_.rank(~last_ts);
    hits_urd_[cur_urd]++;
    uint64_t last_urd = blk->lastURD;
    if(cur_urd < last_urd) {
      num_streaks_urd_[last_urd]++;
      num_streaks_urd_[cur_urd]--;
    }

    uint64_t cur_ard = total_accesses_ - last_ts;
    hits_ard_[cur_ard]++;
    uint64_t last_ard = blk->lastARD;
    if (cur_ard < last_ard) {
      num_streaks_ard_[last_ard]++;
      num_streaks_ard_[cur_ard]--;
    }

    // update
    blk->lastTS = total_accesses_;
    blk->lastURD = cur_urd;
    blk->lastARD = cur_ard;
    stack_.erase(~last_ts);
    stack_.insert(~total_accesses_, blk);
    return true;
  } else {

    // insert new tag
    auto blk = make_shared<Block>(Block{tag, UINT64_MAX, UINT64_MAX, total_accesses_});
    cache_map_[tag] = blk;
	stack_.insert(~total_accesses_, blk);
    return false;
  }
}


void
Profiler::outputMissesByURD(ostream & out) {

  // csv header
  out<<"unique_reuse_distance"<<","<<"num_misses"<<endl;
  uint64_t num_misses = total_accesses_;
  out << 0 << "," << num_misses << endl;
  for(auto it=begin(hits_urd_); it!=end(hits_urd_); ++it) {
    num_misses -= (*it).second;
    out<<((*it).first+1)<<","<<num_misses<<endl;
  }
}

void
Profiler::outputMissesByARD(ostream & out) {
  // csv header
  out<<"absolute_reuse_distance"<<","<<"num_misses"<<endl;
  uint64_t num_misses = total_accesses_;
  out << 0 << "," << num_misses << endl;
  for(auto it=begin(hits_ard_); it!=end(hits_ard_); ++it) {
    num_misses -= (*it).second;
    out<<(*it).first<<","<<num_misses<<endl;
  }
}

void
Profiler::outputNumStreaksByURD(ostream & out) {
  //csv header
  out<<"unique_reuse_distance"<<","<<"num_streaks"<<endl;
  int64_t num_streaks = 0;
  for (auto it=rbegin(num_streaks_urd_); it!=rend(num_streaks_urd_); ++it) {
    assert(num_streaks>=0);
    num_streaks += (*it).second;
    out<<(*it).first<<","<<num_streaks<<endl;
  }
}

void
Profiler::outputNumStreaksByARD(ostream & out) {
  //csv header
  out<<"absolute_reuse_distance"<<","<<"num_streaks"<<endl;
  int64_t num_streaks = 0;
  for (auto it=rbegin(num_streaks_ard_); it!=rend(num_streaks_ard_); ++it) {
    assert(num_streaks>=0);
    num_streaks += (*it).second;
    out<<((*it).first-1)<<","<<num_streaks<<endl;
  }
}


