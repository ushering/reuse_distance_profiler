//
// Created by Ruisheng Wang on 2/28/16.
//

#include "trace_reader.hh"



#include <boost/iostreams/filter/gzip.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <fstream>
#include <memory>
#include <vector>


using namespace std;

TraceReader::TraceReader(const string trace_file_path)
  : m_trace_infile(trace_file_path.c_str(), ios_base::in | ios_base::binary) {

  namespace bi = boost::iostreams;
  m_trace_stream.push(bi::gzip_decompressor());
  m_trace_stream.push(m_trace_infile);

}

bool
TraceReader::read(Request &req) {

  string line;
  if (getline(m_trace_stream, line)) {

    vector<string> fields;
    boost::split(fields,line,boost::is_any_of(","));

    assert(fields.size() == 6);  // should have six fields: cycle, tag, pc, instSeqId, acsSeqId, nextRef

    req.cycle = boost::lexical_cast<uint64_t>(fields[0]);
    req.tag = boost::lexical_cast<uint64_t>(fields[1]);
    req.pc = boost::lexical_cast<uint64_t>(fields[2]);
    req.instSeqId = boost::lexical_cast<uint64_t>(fields[3]);
    req.acsSeqId = boost::lexical_cast<uint64_t>(fields[4]);
    req.nextRef = boost::lexical_cast<double>(fields[5]);
    return true;
  }
  return false;
}
