#include <iostream>
#include <fstream>
#include <string>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "profiler.hh"
#include "trace_reader.hh"



using namespace std;

// system variables
string g_out_dir_str;
string g_trace_dir_str;
string g_benchmark_str;


bool
processCommandArgs(int argc, char* argv[]) {

  namespace po = boost::program_options;
  po::options_description desc("Options");

  desc.add_options()
      ("help,h", "produce help message")
      ("trace-dir,t", po::value<string>(&g_trace_dir_str)->required(), "Directory for trace files")
      ("output-dir,o", po::value<string>(&g_out_dir_str)->required(), "Directory for output data")
      ("benchmark,b", po::value<string>(&g_benchmark_str)->required(), "Benchmark Name")
      ;
  try {
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc),vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return false;
    }

    po::notify(vm);

  } catch (boost::program_options::unknown_option & e) {
    std::cerr << e.what() << "\n" << desc << "\n";
    return false;
  } catch (boost::program_options::error& e) {
    std::cerr << e.what() << endl;
    return false;
  }

  return true;
}



int
main(int argc, char *argv[]) {

  if(!processCommandArgs(argc, argv)) {
    return -1;
  }

  namespace bf = boost::filesystem;

  bf::path trace_dir_path(g_trace_dir_str);

  bf::path trace_file_path = trace_dir_path / (g_benchmark_str + ".csv.gz");

  if (!bf::exists(trace_file_path)) {
    cerr<<trace_file_path.string()<<" does not exist!"<<endl;
    return -1;
  }

  TraceReader trace_stream(trace_file_path.string());
  Request req;

  Profiler profiler;

  cout<<"Begin Profililng ..."<<endl;
  while(trace_stream.read(req)) {
    profiler.access(req.tag);
  }

  // output results

  bf::path output_dir_path(g_out_dir_str);

  if(!bf::exists(output_dir_path)) {
    if(!bf::create_directories(output_dir_path)){
      cerr<<"Cannot create output directory!"<<endl;
      return -1;
    }
  }

  cout<<"Output Profiled Data ..."<<endl;

  bf::path out_file1_path = output_dir_path / (g_benchmark_str + "_urd_miss_curve.csv");
  ofstream out_file1(out_file1_path.string());
  profiler.outputMissesByURD(out_file1);
  out_file1.close();

  bf::path out_file2_path = output_dir_path / (g_benchmark_str + "_ard_miss_curve.csv");
  ofstream out_file2(out_file2_path.string());
  profiler.outputMissesByARD(out_file2);
  out_file2.close();

  bf::path out_file3_path = output_dir_path / (g_benchmark_str + "_urd_num_streaks.csv");
  ofstream out_file3(out_file3_path.string());
  profiler.outputNumStreaksByURD(out_file3);
  out_file3.close();

  bf::path out_file4_path = output_dir_path / (g_benchmark_str + "_ard_num_streaks.csv");
  ofstream out_file4(out_file4_path.string());
  profiler.outputNumStreaksByARD(out_file4);
  out_file4.close();

  return 0;
}
