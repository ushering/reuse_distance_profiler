# Add repository
sudo add-apt-repository -y ppa:george-edison55/cmake-3.x
sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test

# Update software repository
sudo apt-get -y update

# install software
# install cmake 3
sudo apt-get -y install cmake

# install gcc/g++ 5
sudo apt-get install -y gcc-5 g++-5
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 10

# install boost
sudo apt-get install -y libboost-all-dev
