import textwrap
import os
import sys
import subprocess
from optparse import OptionParser
import re

outputDir = "job_outputs"


def submit(command, job_name='job'):
    print(command)

    if not os.path.exists(outputDir):
        os.makedirs(outputDir)

    process = subprocess.Popen(['qsub'],
                               stdin=subprocess.PIPE)
    job_string = """
        #!/bin/bash
        echo "hello from job script!"
        echo "the date is" `date`
        #$ -N %s
        #$ -cwd
        #$ -M ruishenw@usc.edu
        #$ -o %s/$JOB_NAME.o.$JOB_ID
        #$ -e %s/$JOB_NAME.e.$JOB_ID
        #$ -binding linear:1
        %s""" % (job_name, outputDir, outputDir, command)
    process.communicate(input=textwrap.dedent(job_string).encode())   # Note encode() convert string to bytes


def get_all_benchmarks_from_trace_dir(trace_dir):
    for trace_file in os.listdir(trace_dir):
        m = re.match('(.+)\.csv\.gz$', trace_file)
        if m: yield m.group(1)


def main():
    parser = OptionParser()
    parser.add_option("-t", "--trace-dir",
                      action="store", type="string", dest="trace_dir")
    parser.add_option("-o", "--output-dir",
                      action="store", type="string", dest="output_dir")

    parser.add_option("-e", "--executable",
                      action="store", type="string", dest="executable")

    (options, args) = parser.parse_args()

    if not options.trace_dir:
        print("Please sepcifiy trace dir")
        return
    assert(os.path.isdir(options.trace_dir))

    if not options.output_dir:
        print("Please specify output dir")
        return

    if not options.executable:
        print('Please specify executable')
        return
    assert(os.path.isfile(options.executable))

    for benchmark in get_all_benchmarks_from_trace_dir(options.trace_dir):
        command = ' '.join([options.executable, '-t', options.trace_dir, '-b', benchmark, '-o', options.output_dir])
        submit(command)


if __name__ == '__main__':
    main()
